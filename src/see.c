/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   see.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: takiapo <takiapo@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/05 15:45:12 by takiapo           #+#    #+#             */
/*   Updated: 2015/03/11 16:52:42 by takiapo          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/wolf3d.h"

void		deltas(t_perso *him, t_ray *look, t_perso *eye)
{
	int		i;
	double	power[2];

	i = 0;
	power[0] = eye->dir[1] * eye->dir[1];
	power[1] = eye->dir[0] * eye->dir[0];
	while (i < 2)
	{
		eye->pos[i] = him->pos[i];
		eye->dir[i] = him->dir[i] + him->plane[i] * look->view;
		look->grid_pos[i] = (int)him->pos[i];
		look->step[i] = (eye->dir[i] > 0) ? 1 : -1;
		if (eye->dir[i] < 0)
			look->first[i] = (eye->pos[i] - look->grid_pos[i]) * look->delta[i];
		else
			look->first[i] = (look->grid_pos[i] + 1.0
					- eye->pos[i]) * look->delta[i];
		look->delta[i] = sqrt(1 + power[i] / power[i ^ 1]);
		i++;
	}
}

int			intersect(t_ray *look, char **map)
{
	int		hit;
	int		side;

	hit = 0;
	while (hit == 0)
	{
		if (look->first[0] < look->first[1])
		{
			look->first[0] += look->delta[0];
			look->grid_pos[0] += look->step[0];
			side = 0;
		}
		else
		{
			look->first[1] += look->delta[1];
			look->grid_pos[1] += look->step[1];
			side = 1;
		}
		if (!(look->view = map[look->grid_pos[1]][look->grid_pos[0]]))
			exit(1);
		if (look->view != '0')
			hit = 1;
	}
	return (look->color = side);
}

void		draw_wall(double dist, t_libx *mlx, int ray, int side)
{
	void	(*f[2])();
	int		limit[2];
	int		height;

	height = fabs((SIZE_WINY / dist));
	limit[0] = -height + SIZE_WINY / 2;
	limit[1] = height + SIZE_WINY / 2;
	if (limit[1] >= SIZE_WINY)
		limit[1] = SIZE_WINY - 1;
	(f)[0] = v_stripe0;
	(f)[1] = v_stripe1;
	limit[0]--;
	if (limit[0] < 0)
		limit[0] = 0;
	(f)[mlx->img->endian](mlx->img, ray, limit, 826461 / (side + 1));
}

void		reset_image(t_img *img)
{
	int		x;
	int		y;
	int		line;
	int		column;

	y = 0;
	while (y < SIZE_WINY - 1)
	{
		x = 0;
		line = img->size_line * y;
		while (x < SIZE_WINX)
		{
			column = x << 2;
			img->data[line + column + 3] = 0;
			img->data[line + column + 2] = 0;
			img->data[line + column + 1] = 0;
			img->data[line + column + 0] = 0;
			x++;
		}
		y++;
	}
}

void		engine(t_all *data)
{
	t_ray	look;
	t_perso	eye;
	int		ray;
	double	dist;

	ray = 0;
	reset_image(data->mlx->img);
	while (ray < SIZE_WINX)
	{
		look.view = (double)((2 * ray / (double)SIZE_WINX) - 1);
		deltas(data->him, &look, &eye);
		if (intersect(&look, data->map) == 0)
			dist = fabs((look.grid_pos[0]
						- eye.pos[0] + (1 - look.step[0]) / 2) / eye.dir[0]);
		else
			dist = fabs((look.grid_pos[1]
						- eye.pos[1] + (1 - look.step[1]) / 2) / eye.dir[1]);
		draw_wall(dist, data->mlx, ray, look.color);
		ray++;
	}
	mlx_put_image_to_window(data->mlx->mlx_ptr, data->mlx->win,
			data->mlx->img->img_ptr, 0, 0);
}
