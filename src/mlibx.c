/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mlibx.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: takiapo <takiapo@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/02/22 20:29:15 by takiapo           #+#    #+#             */
/*   Updated: 2015/02/22 20:30:40 by takiapo          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/wolf3d.h"

t_libx		*init_mlx(t_libx *init, int size_x, int size_y)
{
	t_libx	*temp;

	temp = malloc(sizeof(t_libx));
	if (temp == NULL)
		return (NULL);
	temp->mlx_ptr = mlx_init();
	temp->win = mlx_new_window(temp->mlx_ptr, size_x, size_y, "wolf32");
	temp->img = new_image(temp->mlx_ptr, SIZE_WINX, SIZE_WINY);
	if (!temp->img)
		return (NULL);
	init = temp;
	return (init);
}

t_img		*new_image(void *mlx_ptr, int size_x, int size_y)
{
	t_img	*image;

	image = malloc(sizeof(t_img));
	if (image == NULL)
		return (NULL);
	image->img_ptr = mlx_new_image(mlx_ptr, size_x, size_y);
	image->data = mlx_get_data_addr(image->img_ptr, &(image->bits_per_pixel),
			&(image->size_line), &(image->endian));
	return (image);
}
