/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   touch.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: takiapo <takiapo@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/09 18:17:08 by takiapo           #+#    #+#             */
/*   Updated: 2015/03/11 16:46:34 by takiapo          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/wolf3d.h"

void			up(t_all *data)
{
	t_perso		*him;

	him = data->him;
	if (data->map[(int)(him->pos[0] + him->dir[0] * 0.3)][(int)(him->pos[1])]
			== '0')
		him->pos[0] += him->dir[0] * 0.3;
	if (data->map[(int)(him->pos[0])][(int)(him->pos[1] + him->dir[1] * 0.3)]
			== '0')
		him->pos[1] += him->dir[1] * 0.3;
	engine(data);
}

void			down(t_all *data)
{
	t_perso		*him;

	him = data->him;
	if (data->map[(int)(him->pos[0] - him->dir[0] * 0.3)][(int)(him->pos[1])]
			== '0')
		him->pos[0] -= him->dir[0] * 0.3;
	if (data->map[(int)(him->pos[0])][(int)(him->pos[1] - him->dir[1] * 0.3)]
			== '0')
		him->pos[1] -= him->dir[1] * 0.3;
	engine(data);
}

void			right(t_all *data)
{
	t_perso		*him;
	double		olddirx;
	double		oldplanex;

	him = data->him;
	olddirx = him->dir[0];
	oldplanex = him->plane[0];
	him->dir[0] = him->dir[0] * cos(-ROT_SPEED) - him->dir[1] * sin(-ROT_SPEED);
	him->dir[1] = olddirx * sin(-ROT_SPEED) + him->dir[1] * cos(-ROT_SPEED);
	him->plane[0] = him->plane[0] * cos(-ROT_SPEED)
		- him->plane[1] * sin(-ROT_SPEED);
	him->plane[1] = oldplanex * sin(-ROT_SPEED)
		+ him->plane[1] * cos(-ROT_SPEED);
	engine(data);
}

void			left(t_all *data)
{
	t_perso		*him;
	double		olddirx;
	double		oldplanex;

	him = data->him;
	olddirx = him->dir[0];
	oldplanex = him->plane[0];
	him->dir[0] = him->dir[0] * cos(ROT_SPEED) - him->dir[1] * sin(ROT_SPEED);
	him->dir[1] = olddirx * sin(ROT_SPEED) + him->dir[1] * cos(ROT_SPEED);
	him->plane[0] = him->plane[0] * cos(ROT_SPEED)
		- him->plane[1] * sin(ROT_SPEED);
	him->plane[1] = oldplanex * sin(ROT_SPEED)
		+ him->plane[1] * cos(ROT_SPEED);
	engine(data);
}
