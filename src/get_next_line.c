/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: takiapo <takiapo@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/03 09:15:38 by takiapo           #+#    #+#             */
/*   Updated: 2014/11/05 22:46:34 by takiapo          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/get_next_line.h"

static int		str_end(char *s, char c)
{
	int			i;

	i = 0;
	if (!s)
		return (0);
	while (s[i])
	{
		if (s[i] == c)
		{
			if (i == 0)
				i++;
			return (i);
		}
		i++;
	}
	if (c != 0)
		return (-1);
	return (i);
}

char			*ft_strjoin_del(char **s1, char *s2)
{
	char		*p;
	char		*p2;
	char		*s3;

	s3 = *s1;
	if (s1 == NULL && s2 == NULL)
		return (NULL);
	p = (char *)malloc((str_end(s3, 0) + str_end(s2, 0)) * sizeof(char) + 1);
	p2 = p;
	if (p == NULL)
		return (NULL);
	if (s3)
	{
		while (*s3)
			*p++ = *s3++;
	}
	while (*s2)
		*p++ = *s2++;
	*p++ = '\0';
	if (*s1)
	{
		free(*s1);
		*s1 = NULL;
	}
	return (p2);
}

char			*line_check(char *wrote, char *buf, char **left_over)
{
	char		*line;
	size_t		i;

	i = 0;
	line = (char *)malloc(sizeof(char) * (str_end(buf, 0) + 1));
	if (line == NULL)
		return (NULL);
	while (buf[i] != '\n')
	{
		line[i] = buf[i];
		i++;
	}
	line[i] = '\0';
	wrote = ft_strjoin_del(&wrote, line);
	free(line);
	line = NULL;
	line = ft_strjoin_del(&line, &buf[i + 1]);
	free(*left_over);
	*left_over = line;
	return (wrote);
}

char			*get_line(int const fd, char *stored_line, int *ret)
{
	char		*buf;
	static char	*left_over = NULL;

	if (left_over != NULL)
	{
		if (str_end(left_over, '\n') > 0)
		{
			stored_line = line_check(stored_line, left_over, &left_over);
			return (stored_line);
		}
		stored_line = ft_strjoin_del(&stored_line, left_over);
	}
	buf = (char *)malloc(sizeof(char) * BUFF_SIZE + 1);
	if (buf == NULL)
		return (NULL);
	while ((*ret = read(fd, buf, BUFF_SIZE)) > 0)
	{
		buf[*ret] = '\0';
		if (str_end(buf, '\n') > 0)
			return (stored_line = line_check(stored_line, buf, &left_over));
		else
			stored_line = ft_strjoin_del(&stored_line, buf);
	}
	free(buf);
	return (stored_line);
}

int				get_next_line(int const fd, char **line)
{
	char		*stored_line;
	int			ret;

	ret = 1;
	if (fd == -1)
		return (-1);
	stored_line = NULL;
	stored_line = get_line(fd, stored_line, &ret);
	*line = stored_line;
	if (ret == 0 && !stored_line)
		return (0);
	if (stored_line == NULL || ret == -1)
		return (-1);
	return (1);
}
