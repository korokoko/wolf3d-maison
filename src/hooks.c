/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   hooks.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: takiapo <takiapo@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/05 21:03:04 by takiapo           #+#    #+#             */
/*   Updated: 2015/03/11 16:33:26 by takiapo          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/wolf3d.h"

void		add_point(t_img *img, int x, int y, int color)
{
	if (img->endian == 0)
	{
		img->data[y * img->size_line + x * 4 + 3] = (color & 0xFF000000) >> 24;
		img->data[y * img->size_line + x * 4 + 2] = (color & 0xFF0000) >> 16;
		img->data[y * img->size_line + x * 4 + 1] = (color & 0xFF00) >> 8;
		img->data[y * img->size_line + x * 4 + 0] = (color & 0xFF);
	}
	else if (img->endian == 1)
	{
		img->data[y * img->size_line + x * 4 + 0] = (color & 0xFF000000) >> 24;
		img->data[y * img->size_line + x * 4 + 1] = (color & 0xFF0000) >> 16;
		img->data[y * img->size_line + x * 4 + 2] = (color & 0xFF00) >> 8;
		img->data[y * img->size_line + x * 4 + 3] = (color & 0xFF);
	}
}

int			wolf3d(t_all *data)
{
	engine(data);
	return (0);
}

int			expose_hook(t_all *data)
{
	engine(data);
	return (0);
}

int			key_hook(int keycode, t_all *data)
{
	(void)data;
	if (keycode == 65362)
		up(data);
	if (keycode == 65364)
		down(data);
	if (keycode == 65363)
		right(data);
	if (keycode == 65361)
		left(data);
	if (keycode == 65307)
		exit(0);
	return (0);
}
