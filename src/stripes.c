/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stripes.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: takiapo <takiapo@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/02/23 08:02:42 by takiapo           #+#    #+#             */
/*   Updated: 2015/03/11 16:54:05 by takiapo          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/wolf3d.h"

void		v_stripe0(t_img *img, int x, int y[2], int color)
{
	int		line;
	int		column;

	column = x << 2;
	while (y[0] < y[1])
	{
		line = y[0] * img->size_line;
		img->data[line + column + 3] = (color & 0xFF000000) >> 24;
		img->data[line + column + 2] = (color & 0xFF0000) >> 16;
		img->data[line + column + 1] = (color & 0xFF00) >> 8;
		img->data[line + column + 0] = (color & 0xFF);
		y[0]++;
	}
}

void		v_stripe1(t_img *img, int x, int y[2], int color)
{
	int		line;
	int		column;

	column = x << 2;
	while (y[0] < y[1])
	{
		line = y[0] * img->size_line;
		img->data[line + column + 0] = (color & 0xFF000000) >> 24;
		img->data[line + column + 1] = (color & 0xFF0000) >> 16;
		img->data[line + column + 2] = (color & 0xFF00) >> 8;
		img->data[line + column + 3] = (color & 0xFF);
		y[0]++;
	}
}
