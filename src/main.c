/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: takiapo <takiapo@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/04 16:20:21 by takiapo           #+#    #+#             */
/*   Updated: 2015/03/11 16:54:53 by takiapo          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/wolf3d.h"

void		get_sides(int color[4])
{
	color[4] = NORTH;
	color[5] = WEST;
	color[1] = WEST;
	color[2] = SOUTH;
	color[3] = EAST;
	return ;
}

char		**get_map(int fd)
{
	char	**map;
	char	*line;
	int		depth;
	int		i;

	i = 0;
	if (fd < -1)
		return (NULL);
	if (get_next_line(fd, &line) < 0 || !(depth = ft_atoi(line)))
		return (NULL);
	map = malloc(sizeof(char *) * depth);
	if (map == NULL)
		return (NULL);
	map[depth] = NULL;
	while (i < depth)
	{
		get_next_line(fd, &line);
		map[i] = line;
		i++;
	}
	return (map);
}

t_perso		*new_perso(t_perso *him)
{
	t_perso *temp;

	temp = malloc(sizeof(t_perso));
	if (!temp)
		return (NULL);
	temp->pos[0] = START_POS_X;
	temp->pos[1] = START_POS_Y;
	temp->plane[0] = 0;
	temp->plane[1] = 0.66;
	temp->dir[0] = -1;
	temp->dir[1] = 0;
	him = temp;
	return (him);
}

int			main(int ac, char **av)
{
	t_all	*data;
	int		fd;

	(void)ac;
	(void)av;
	data = malloc(sizeof(t_all));
	if (!data)
		return (EXIT_FAILURE);
	fd = open("map", O_RDONLY);
	data->map = get_map(fd);
	data->mlx = init_mlx(data->mlx, SIZE_WINX, SIZE_WINY);
	data->him = new_perso(data->him);
	data->old = NULL;
	get_sides(data->color);
	if (!data->map || !data->mlx || !data->him)
		return (EXIT_FAILURE);
	mlx_hook(data->mlx->win, KEYPRESS, KEYPRESSMASK, key_hook, data);
	mlx_expose_hook(data->mlx->win, expose_hook, data);
	mlx_loop(data->mlx->mlx_ptr);
	return (EXIT_SUCCESS);
}
