/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   wolf3D.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: takiapo <takiapo@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/04 16:46:40 by takiapo           #+#    #+#             */
/*   Updated: 2015/03/11 16:53:46 by takiapo          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef WOLF3D_H
# define WOLF3D_H

# include <mlx.h>
# include <math.h>
# include <fcntl.h>
# include <stdlib.h>
# include <unistd.h>
# include <stdio.h>
# include "struct_wolf.h"
# include "../libft/includes/libft.h"

# define KEYPRESSMASK (1L<<0)
# define KEYPRESS 2
# define START_POS_X 22
# define START_POS_Y 10
# define START_SQUARE_X 10
# define START_SQUARE_Y 7
# define START_DIR 90
# define SIZE_WINX 920
# define SIZE_WINY 600
# define ROT_SPEED 0.2
# define NORTH 6708510
# define SOUTH 8890731
# define EAST 12375270
# define WEST 11714229

int			expose_hook(t_all *daya);
int			key_hook(int keycode, t_all *data);
int			get_next_line(int fd, char **line);
int			wolf3d(t_all *data);
t_img		*new_image(void *mlx_ptr, int size_x, int size_y);
t_libx		*init_mlx(t_libx *init, int size_x, int size_y);
void		add_point(t_img *img, int x, int y, int color);
void		v_stripe0(t_img *img, int x, int y[2], int color);
void		v_stripe1(t_img *img, int x, int y[2], int color);
double		sign(double ret);
void		engine(t_all *data);
void		up(t_all *data);
void		down(t_all *data);
void		right(t_all *data);
void		left(t_all *data);
#endif
