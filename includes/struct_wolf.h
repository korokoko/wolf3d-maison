/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   struct_wolf.h                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: takiapo <takiapo@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/04 16:50:00 by takiapo           #+#    #+#             */
/*   Updated: 2015/02/24 01:02:49 by takiapo          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef STRUCT_WOLF_H
# define STRUCT_WOLF_H

typedef struct	s_coord
{
	double		x;
	double		y;
}				t_coord;

typedef struct	s_coop
{
	int			x;
	int			y;
}				t_coop;

typedef	struct	s_img
{
	void		*img_ptr;
	int			bits_per_pixel;
	int			size_line;
	int			endian;
	char		*data;
}				t_img;

typedef struct	s_libx
{
	void		*mlx_ptr;
	void		*win;
	t_img		*img;
}				t_libx;

typedef struct	s_perso
{
	double		pos[2];
	double		plane[2];
	double		dir[2];
}				t_perso;

typedef struct	s_ray
{
	double		delta[2];
	double		step[2];
	double		first[2];
	int			grid_pos[2];
	int			color;
	double		view;
}				t_ray;

typedef struct	s_all
{
	t_libx		*mlx;
	char		**map;
	t_perso		*him;
	t_img		*old;
	int			color[5];
}				t_all;

#endif
