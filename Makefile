# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: rmichelo <rmichelo@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2014/11/09 10:49:51 by rmichelo          #+#    #+#              #
#    Updated: 2015/03/11 16:59:25 by takiapo          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

CC				=	gcc
NAME			=	wolf3d
SRC				=	src/main.c \
					src/touch.c \
					src/see.c \
					src/mlibx.c \
					src/get_next_line.c \
					src/stripes.c\
					src/hooks.c

OBJ				=	$(SRC:.c=.o)
INCLUDE			=	-I/usr/X11R6/include -I/opt/X11/include
CFLAGS			=	-Wextra -Wall -Werror -g -pg
RM				=	rm -rf
BEGIN_COMPIL	=	@echo " Compilation launched."
END_COMPIL		=	@echo " Compilation ended."
BEGIN_CLEAN		=	@echo " Clean launched."
END_CLEAN		=	@echo " Clean ended."
BEGIN_SAVE		=	@echo " Save launched."
END_SAVE		=	@echo " Save ended."
SAVE_NAME		=	save_wolf3d.tar.gz
SAVE_INC_NAME	=	wolf3d.h
SAVE			=	tar cfz $(SAVE_NAME) Makefile $(SAVE_INC_NAME) $(SRC)
MINILIBX		=	-L/usr/X11/lib -lmlx -lX11 -lXext
LIBMATHS		=	-lm
LIBFTDIR		=	libft/
LIBFTH			=	-I$(LIBFTDIR)
LIBFTFLAGS		=	-L$(LIBFTDIR) -lft

$(NAME):	 makelibft $(OBJ)
		$(CC) $(CFLAGS) $(MINILIBX) $(LIBMATHS) $(INCLUDE) -o $(NAME) $(OBJ) $(LIBFTH) $(LIBFTFLAGS) $(MINILIBX) $(LIBMATHS)

makelibft:
	make -C $(LIBFTDIR)

makerelibft:
	make -C $(LIBFTDIR) re

all:		$(NAME)

end_compil:
		@echo "\033[1;35m"
		$(END_COMPIL)
		@echo "\033[00m"
		@echo ""

echo_name:
		@echo "\033[1;32m"
		@echo ""
		@echo "  __      __      .__   _____  ________ ________    "
		@echo " /  \    /  \____ |  |_/ ____\ \_____  \\\______ \   "
		@echo " \   \/\/   /  _ \|  |\   __\    _(__  < |    |  \  "
		@echo "  \        (  <_> )  |_|  |     /       \|    \`   \ "
		@echo "   \__/\  / \____/|____/__|    /______  /_______  / "
		@echo "        \/                            \/        \/  "
		@echo "\033[1;33m"

begin_compil:
		@echo ""
		@echo "\033[1;35m"
		$(BEGIN_COMPIL)
		@echo "\033[1;33m"

save:
		@echo "\033[1;35m"
		$(BEGIN_SAVE)
		@echo "\033[1;33m"

		$(RM) $(SAVE_NAME)
		$(SAVE)

		@echo "\033[1;35m"
		$(END_SAVE)
		@echo "\033[00m"

clean:
		@echo ""
		@echo "\033[1;35m"
		$(BEGIN_CLEAN)
		@echo "\033[1;33m"

		$(RM) $(OBJ)

		@echo "\033[1;35m"
		$(END_CLEAN)
		@echo "\033[00m"

fclean:
		@echo ""
		@echo "\033[1;35m"
		$(BEGIN_CLEAN)
		@echo "\033[1;33m"

		$(RM) $(OBJ)

		@echo "\033[1;35m"
		$(END_CLEAN)
		@echo "\033[1;33m"

		$(RM) $(NAME)

		@echo "\033[1;35m"
		@echo " Old binary deleted."
		@echo "\033[00m"

end:
		@echo "\033[1;32m"
		@echo " Installation successful."
		@echo "\033[00m"

re:		echo_name fclean makerelibft begin_compil all end_compil end

.PHONY:		all clean re echo_name begin_compil end_compil save end
